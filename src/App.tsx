import {flatten} from 'lodash'
import React, {useEffect, useState} from 'react'
import {fetchTenBlocks} from './api'

const App = () => {
  const [transferEvents, setTransferEvents] = useState<any>([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    fetchTenBlocks()
      .then((transactions) => {
        setTransferEvents(flatten(transactions))
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])
  console.log(transferEvents)
  return (
    <div
      style={{
        padding: '0em 10em',
      }}
    >
      <header className='App-header'>
        <p>Browse ERC-20 transfer events</p>
      </header>
      <table style={{width: '100%'}}>
        <thead>
          <tr>
            <th>Transaction Hash</th>
            <th>From</th>
            <th>To</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {transferEvents.map((transferEvent: any) => (
            <tr key={transferEvent.hash}>
              <td>{transferEvent.hash}</td>
              <td>{transferEvent.from}</td>
              <td>{transferEvent.to}</td>
              <td>{parseInt(transferEvent.value) / 1e18} eth</td>
            </tr>
          ))}
          {loading && <span>Loading...</span>}
        </tbody>
      </table>
    </div>
  )
}

export default App
