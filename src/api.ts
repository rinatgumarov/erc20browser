import {axios} from './axios'

export const fetchBlock = async (block: string) => {
  const blockResult = await axios.post('', {
    jsonrpc: '2.0',
    id: Math.random().toString(),
    method: 'eth_getBlockByNumber',
    params: [block, false],
  })
  return blockResult?.data
}

export const fetchTenBlocks = async (after?: number) => {
  const lastBlockNumber = after
    ? after - 1
    : parseInt(
        (
          await axios.post('', {
            jsonrpc: '2.0',
            id: Math.random().toString(),
            method: 'eth_blockNumber',
            params: [],
          })
        ).data.result,
      )
  const blockNumbers = new Array(10)
    .fill(undefined)
    .map((e, index) => lastBlockNumber - index)
  return Promise.all(
    blockNumbers.map((blockNumber) =>
      fetchBlockTransactions(`0x${blockNumber.toString(16)}`),
    ),
  )
}

export const fetchTransaction = async (transaction: string) => {
  const transactionResult = await axios.post('', {
    jsonrpc: '2.0',
    id: Math.random().toString(),
    method: 'eth_getTransactionByHash',
    params: [transaction],
  })
  return transactionResult?.data
}

export const fetchBlockTransactions = async (block: string) => {
  const transactions = (await fetchBlock(block))?.result?.transactions
  return (await Promise.all(transactions.map(fetchTransaction))).map(
    (t) => (t as any).result,
  )
}
