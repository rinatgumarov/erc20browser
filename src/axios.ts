import pureAxios from 'axios'
export const axios = pureAxios.create({
  baseURL: 'https://mainnet.infura.io/v3/78f879364d86400cbb710b55c43106e7',
  headers: {
    'Content-Type': 'application/json',
  },
})
